import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import './App.css';
import firebase from './Firebase';

class App extends Component {
  constructor(props) {
    super(props);
    this.ref = firebase.database().ref('notes');
    this.state = {
      notes: []
    };

    this.onCollectionUpdate = this.onCollectionUpdate.bind(this);
  }

  onCollectionUpdate() {
    this.ref.on('value', (snapshot) => {
      if (snapshot.val() == null) {
        return null
      } else {
        let notes = [];
        snapshot.forEach((data) => {
          let note = {
            id: data.key,
            title: data.val().title,
            content: data.val().content,
            author: data.val().author,
            comments: []
          };
          notes.push(note);
        });
        console.log(notes);
        this.setState({
          notes: notes
        });
      }
    });
  };

  componentDidMount() {
    this.onCollectionUpdate();
  };

  render() {
    return (
      <div className="container">
        <div className="panel panel-default">
          <div className="panel-heading">
            <h3 className="panel-title">
              NOTE LIST
            </h3>
          </div>
          <div className="panel-body">
            <h4><Link to="/create-note" className="btn btn-primary">Add Note</Link></h4>
            <table className="table table-stripe">
              <thead>
                <tr>
                  <th>Title</th>
                  <th>Note</th>
                  <th>Author</th>
                </tr>
              </thead>
              <tbody>
              { this.state.notes.map((note) =>
                <tr key={note.id}>
                  <td>
                    <Link to={`/note/${note.id}`}>{note.title}</Link>
                  </td>
                  <td>{note.content}</td>
                  <td>{note.author}</td>
                </tr>
              )
              }
              </tbody>
            </table>
          </div>
        </div>
      </div>
    );
  }
}

export default App;
