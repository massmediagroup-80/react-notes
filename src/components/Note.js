import React, { Component } from 'react';
import firebase from '../Firebase';
import { Link } from 'react-router-dom';

class Note extends Component {

  constructor(props) {
    super(props);
    this.state = {
      note: {},
      key: ''
    };
  }

  componentDidMount() {
    const ref = firebase.database().ref('notes');
    ref.on('value',(snapshot) => {
      const data = snapshot.val();
      let snapshotKey = snapshot.child(this.props.match.params.id).key;

      this.setState({
        note: data[snapshotKey],
        key: snapshotKey
      });
    });
  }

  delete(id){
    firebase.database().ref('notes').child(id).remove();
  }

  render() {
    return (
      <div className="container">
        <div className="panel panel-default">
          <div className="panel-heading">
          <h4><Link to="/">Notes List</Link></h4>
            <h3 className="panel-title">
              {this.state.note.title}
            </h3>
          </div>
          <div className="panel-body">
            <dl>
              <dt>Note:</dt>
              <dd>{this.state.note.content}</dd>
              <dt>Author:</dt>
              <dd>{this.state.note.author}</dd>
            </dl>
            <Link to={`/edit/${this.state.key}`} className="btn btn-success">Edit</Link>
            <button onClick={this.delete.bind(this, this.state.key)} className="btn btn-danger">Delete</button>
          </div>
        </div>
      </div>
    );
  }
}

export default Note;
