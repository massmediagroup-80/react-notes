import React, { Component } from 'react';
import firebase from '../Firebase';
import { Link } from 'react-router-dom';

class EditNote extends Component {

  constructor(props) {
    super(props);
    this.state = {
      key: '',
      title: '',
      content: '',
      author: ''
    };
  }

  componentDidMount() {
    const ref = firebase.database().ref('notes');
    ref.on('value',(snapshot) => {
      const data = snapshot.val();
      let snapshotKey = snapshot.child(this.props.match.params.id).key;

      this.setState({
        key: snapshotKey,
        title: data[snapshotKey].title,
        content: data[snapshotKey].content,
        author: data[snapshotKey].author
      });
    });

  }

  onChange = (e) => {
    const state = this.state;
    state[e.target.name] = e.target.value;
    this.setState({
      note: state
    });
  };

  onSubmit = (e) => {
    e.preventDefault();

    const { title, content, author } = this.state;

    const updateRef = firebase.database().ref('notes').child(this.state.key);
    updateRef.set({
      title,
      content,
      author
    }).then((docRef) => {
      this.setState({
        key: '',
        title: '',
        content: '',
        author: ''
      });
      this.props.history.push("/note/"+this.props.match.params.id)
    })
    .catch((error) => {
      console.error("Error adding document: ", error);
    });
  };

  render() {
    return (
      <div className="container">
        <div className="panel panel-default">
          <div className="panel-heading">
            <h3 className="panel-title">
              EDIT NOTE
            </h3>
          </div>
          <div className="panel-body">
            <h4><Link to={`/`} className="btn btn-primary">Note List</Link></h4>
            <form onSubmit={this.onSubmit}>
              <div className="form-group">
                <label>Title:</label>
                <input type="text" className="form-control" name="title" value={this.state.title} onChange={this.onChange} placeholder="Title" />
              </div>
              <div className="form-group">
                <label>Note:</label>
                <input type="text" className="form-control" name="content" value={this.state.content} onChange={this.onChange} placeholder="Note" />
              </div>
              <div className="form-group">
                <label>Author:</label>
                <input type="text" className="form-control" name="author" value={this.state.author} onChange={this.onChange} placeholder="Author" />
              </div>
              <button type="submit" className="btn btn-success">Submit</button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default EditNote;
