import React, { Component } from 'react';
import firebase from '../Firebase';
import { Link, withRouter } from 'react-router-dom';

class CreateNote extends Component {
  constructor() {
    super();
    this.ref = firebase.database().ref('notes');
    this.state = {
      title: '',
      content: '',
      author: ''
    };

    this.onSubmit = this.onSubmit.bind(this);
  }

  // componentDidMount () {
  //   this.db = firebase.database();
  // }

  onChange = (e) => {
    const state = this.state;
    state[e.target.name] = e.target.value;
    this.setState(state);
  };

  onSubmit (e) {
    e.preventDefault();
    this.ref.push({
      title: this.state.title,
      content: this.state.content,
      author: this.state.author,
    }).then(() => {
      this.props.history.push('/')
    });
  };

  render() {
    const { title, content, author } = this.state;
    return (
      <div className="container">
        <div className="panel panel-default">
          <div className="panel-heading">
            <h3 className="panel-title">
              ADD NOTE
            </h3>
          </div>
          <div className="panel-body">
            <h4><Link to="/" className="btn btn-primary">Note List</Link></h4>
            <form onSubmit={this.onSubmit}>
              <div className="form-group">
                <label>Title:</label>
                <input type="text" className="form-control" name="title" value={title} onChange={this.onChange} placeholder="Title" required />
              </div>
              <div className="form-group">
                <label>Note:</label>
                <textarea className="form-control" name="content" onChange={this.onChange} placeholder="Note" cols="80" rows="3" value={content} required />
              </div>
              <div className="form-group">
                <label>Author:</label>
                <input type="text" className="form-control" name="author" value={author} onChange={this.onChange} placeholder="Author" required />
              </div>
              <button type="submit" className="btn btn-success">Create note</button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default withRouter(CreateNote);
